import { createStackNavigator } from 'react-navigation';
import HomeScreen from './js/home';
import DetailsScreen from './js/detail';
import React from 'react';

const RootStack = createStackNavigator(
    {
        HomeScreen: HomeScreen,
        DetailsScreen: DetailsScreen,
    },
    {
        initialRouteName: 'HomeScreen',
    }
);
export default class App extends React.Component {
    render() {
        return <RootStack />;
    }
}