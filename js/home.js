import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Image,
    FlatList,
    ScrollView
} from 'react-native';

import {SafeAreaView} from 'react-navigation';

const data = [
    {
        name: 'Túi máy ảnh Benro SAC A-B1 S',
        link: 'https://www.vatgia.com/4814/2301859/t%C3%BAi-m%C3%A1y-%E1%BA%A3nh-benro-sac-a-b1-s.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/dnRvMTM0MTQ4MzkwNy5qcGc-/tui-may-anh-benro-sac-a-b1-s.jpg',
        price: '1.700.000₫'
    },
    {
        name: 'Nikon 1 S1 (1 Nikkor 10-30mm F3.5-5.6 VR) Lens Kit',
        link: 'https://touch.vatgia.com/407/2816587/nikon-1-s1-1-nikkor-10-30mm-f3-5-5-6-vr-lens-kit.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/dXRpMTM1NzcwNTM0Mi5qcGc-/nikon-1-s1-1-nikkor-10-30mm-f3-5-5-6-vr-lens-kit.jpg',
        price: '6.500.000₫'
    },
    {
        name: 'Canon PowerShot G9 X Silver',
        link: 'https://www.vatgia.com/319/5430794/canon-powershot-g9-x-silver.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/bWF0MTQ0NDgxMTYyNS5qcGc-/canon-powershot-g9-x-silver.jpg',
        price: '1.700.000₫'
    },
    {
        name: 'Túi máy ảnh Benro SAC A-B1 S',
        link: 'https://www.vatgia.com/4814/2301859/t%C3%BAi-m%C3%A1y-%E1%BA%A3nh-benro-sac-a-b1-s.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/dnRvMTM0MTQ4MzkwNy5qcGc-/tui-may-anh-benro-sac-a-b1-s.jpg',
        price: '1.700.000₫'
    },
    {
        name: 'Nikon 1 S1 (1 Nikkor 10-30mm F3.5-5.6 VR) Lens Kit',
        link: 'https://www.vatgia.com/407/2816587/nikon-1-s1-1-nikkor-10-30mm-f3-5-5-6-vr-lens-kit.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/dXRpMTM1NzcwNTM0Mi5qcGc-/nikon-1-s1-1-nikkor-10-30mm-f3-5-5-6-vr-lens-kit.jpg',
        price: '6.500.000₫'
    },
    {
        name: 'Canon PowerShot G9 X Silver',
        link: 'https://www.vatgia.com/319/5430794/canon-powershot-g9-x-silver.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/bWF0MTQ0NDgxMTYyNS5qcGc-/canon-powershot-g9-x-silver.jpg',
        price: '1.700.000₫'
    },
    {
        name: 'Túi máy ảnh Benro SAC A-B1 S',
        link: 'https://www.vatgia.com/4814/2301859/t%C3%BAi-m%C3%A1y-%E1%BA%A3nh-benro-sac-a-b1-s.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/dnRvMTM0MTQ4MzkwNy5qcGc-/tui-may-anh-benro-sac-a-b1-s.jpg',
        price: '1.700.000₫'
    },
    {
        name: 'Nikon 1 S1 (1 Nikkor 10-30mm F3.5-5.6 VR) Lens Kit',
        link: 'https://www.vatgia.com/407/2816587/nikon-1-s1-1-nikkor-10-30mm-f3-5-5-6-vr-lens-kit.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/dXRpMTM1NzcwNTM0Mi5qcGc-/nikon-1-s1-1-nikkor-10-30mm-f3-5-5-6-vr-lens-kit.jpg',
        price: '6.500.000₫'
    },
    {
        name: 'Canon PowerShot G9 X Silver',
        link: 'https://www.vatgia.com/319/5430794/canon-powershot-g9-x-silver.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/bWF0MTQ0NDgxMTYyNS5qcGc-/canon-powershot-g9-x-silver.jpg',
        price: '1.700.000₫'
    },
    {
        name: 'Túi máy ảnh Benro SAC A-B1 S',
        link: 'https://www.vatgia.com/4814/2301859/t%C3%BAi-m%C3%A1y-%E1%BA%A3nh-benro-sac-a-b1-s.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/dnRvMTM0MTQ4MzkwNy5qcGc-/tui-may-anh-benro-sac-a-b1-s.jpg',
        price: '1.700.000₫'
    },
    {
        name: 'Nikon 1 S1 (1 Nikkor 10-30mm F3.5-5.6 VR) Lens Kit',
        link: 'https://www.vatgia.com/407/2816587/nikon-1-s1-1-nikkor-10-30mm-f3-5-5-6-vr-lens-kit.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/dXRpMTM1NzcwNTM0Mi5qcGc-/nikon-1-s1-1-nikkor-10-30mm-f3-5-5-6-vr-lens-kit.jpg',
        price: '6.500.000₫'
    },
    {
        name: 'Canon PowerShot G9 X Silver',
        link: 'https://www.vatgia.com/319/5430794/canon-powershot-g9-x-silver.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/bWF0MTQ0NDgxMTYyNS5qcGc-/canon-powershot-g9-x-silver.jpg',
        price: '1.700.000₫'
    },
    {
        name: 'Túi máy ảnh Benro SAC A-B1 S',
        link: 'https://www.vatgia.com/4814/2301859/t%C3%BAi-m%C3%A1y-%E1%BA%A3nh-benro-sac-a-b1-s.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/dnRvMTM0MTQ4MzkwNy5qcGc-/tui-may-anh-benro-sac-a-b1-s.jpg',
        price: '1.700.000₫'
    },
    {
        name: 'Nikon 1 S1 (1 Nikkor 10-30mm F3.5-5.6 VR) Lens Kit',
        link: 'https://www.vatgia.com/407/2816587/nikon-1-s1-1-nikkor-10-30mm-f3-5-5-6-vr-lens-kit.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/dXRpMTM1NzcwNTM0Mi5qcGc-/nikon-1-s1-1-nikkor-10-30mm-f3-5-5-6-vr-lens-kit.jpg',
        price: '6.500.000₫'
    },
    {
        name: 'Canon PowerShot G9 X Silver',
        link: 'https://www.vatgia.com/319/5430794/canon-powershot-g9-x-silver.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/bWF0MTQ0NDgxMTYyNS5qcGc-/canon-powershot-g9-x-silver.jpg',
        price: '1.700.000₫'
    },
    {
        name: 'Túi máy ảnh Benro SAC A-B1 S',
        link: 'https://www.vatgia.com/4814/2301859/t%C3%BAi-m%C3%A1y-%E1%BA%A3nh-benro-sac-a-b1-s.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/dnRvMTM0MTQ4MzkwNy5qcGc-/tui-may-anh-benro-sac-a-b1-s.jpg',
        price: '1.700.000₫'
    },
    {
        name: 'Nikon 1 S1 (1 Nikkor 10-30mm F3.5-5.6 VR) Lens Kit',
        link: 'https://www.vatgia.com/407/2816587/nikon-1-s1-1-nikkor-10-30mm-f3-5-5-6-vr-lens-kit.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/dXRpMTM1NzcwNTM0Mi5qcGc-/nikon-1-s1-1-nikkor-10-30mm-f3-5-5-6-vr-lens-kit.jpg',
        price: '6.500.000₫'
    },
    {
        name: 'Canon PowerShot G9 X Silver',
        link: 'https://www.vatgia.com/319/5430794/canon-powershot-g9-x-silver.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/bWF0MTQ0NDgxMTYyNS5qcGc-/canon-powershot-g9-x-silver.jpg',
        price: '1.700.000₫'
    },
    {
        name: 'Túi máy ảnh Benro SAC A-B1 S',
        link: 'https://www.vatgia.com/4814/2301859/t%C3%BAi-m%C3%A1y-%E1%BA%A3nh-benro-sac-a-b1-s.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/dnRvMTM0MTQ4MzkwNy5qcGc-/tui-may-anh-benro-sac-a-b1-s.jpg',
        price: '1.700.000₫'
    },
    {
        name: 'Nikon 1 S1 (1 Nikkor 10-30mm F3.5-5.6 VR) Lens Kit',
        link: 'https://www.vatgia.com/407/2816587/nikon-1-s1-1-nikkor-10-30mm-f3-5-5-6-vr-lens-kit.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/dXRpMTM1NzcwNTM0Mi5qcGc-/nikon-1-s1-1-nikkor-10-30mm-f3-5-5-6-vr-lens-kit.jpg',
        price: '6.500.000₫'
    },
    {
        name: 'Canon PowerShot G9 X Silver',
        link: 'https://www.vatgia.com/319/5430794/canon-powershot-g9-x-silver.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/bWF0MTQ0NDgxMTYyNS5qcGc-/canon-powershot-g9-x-silver.jpg',
        price: '1.700.000₫'
    },
    {
        name: 'Túi máy ảnh Benro SAC A-B1 S',
        link: 'https://www.vatgia.com/4814/2301859/t%C3%BAi-m%C3%A1y-%E1%BA%A3nh-benro-sac-a-b1-s.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/dnRvMTM0MTQ4MzkwNy5qcGc-/tui-may-anh-benro-sac-a-b1-s.jpg',
        price: '1.700.000₫'
    },
    {
        name: 'Nikon 1 S1 (1 Nikkor 10-30mm F3.5-5.6 VR) Lens Kit',
        link: 'https://www.vatgia.com/407/2816587/nikon-1-s1-1-nikkor-10-30mm-f3-5-5-6-vr-lens-kit.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/dXRpMTM1NzcwNTM0Mi5qcGc-/nikon-1-s1-1-nikkor-10-30mm-f3-5-5-6-vr-lens-kit.jpg',
        price: '6.500.000₫'
    },
    {
        name: 'Canon PowerShot G9 X Silver',
        link: 'https://www.vatgia.com/319/5430794/canon-powershot-g9-x-silver.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/bWF0MTQ0NDgxMTYyNS5qcGc-/canon-powershot-g9-x-silver.jpg',
        price: '1.700.000₫'
    },
    {
        name: 'Túi máy ảnh Benro SAC A-B1 S',
        link: 'https://www.vatgia.com/4814/2301859/t%C3%BAi-m%C3%A1y-%E1%BA%A3nh-benro-sac-a-b1-s.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/dnRvMTM0MTQ4MzkwNy5qcGc-/tui-may-anh-benro-sac-a-b1-s.jpg',
        price: '1.700.000₫'
    },
    {
        name: 'Nikon 1 S1 (1 Nikkor 10-30mm F3.5-5.6 VR) Lens Kit',
        link: 'https://www.vatgia.com/407/2816587/nikon-1-s1-1-nikkor-10-30mm-f3-5-5-6-vr-lens-kit.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/dXRpMTM1NzcwNTM0Mi5qcGc-/nikon-1-s1-1-nikkor-10-30mm-f3-5-5-6-vr-lens-kit.jpg',
        price: '6.500.000₫'
    },
    {
        name: 'Canon PowerShot G9 X Silver',
        link: 'https://www.vatgia.com/319/5430794/canon-powershot-g9-x-silver.html',
        thumbnail: 'https://p.vatgia.vn/ir/pictures_fullsize/8/bWF0MTQ0NDgxMTYyNS5qcGc-/canon-powershot-g9-x-silver.jpg',
        price: '1.700.000₫'
    }
]

function xoa_dau(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    return str;
}

class HomeScreen extends Component {
    state = {
        text: '',
        listData: [],
        keyWord: []
    };

    renderItem=({item})=>{
        return(
            <TouchableOpacity style={styles.containerItem}
              onPress={()=>{
                  this.props.navigation.navigate('DetailsScreen', item);
              }}
            >
                <Image style={styles.thumbnailProduct} source={{uri: item.thumbnail}}/>
                <Text style={styles.textName}>{item.name}</Text>
                <Text style={styles.textPrice}>{item.price}</Text>
            </TouchableOpacity>
        )
    };

    historySearch(){
        return(
            <FlatList
                data={this.state.keyWord}
                keyExtractor={(item, index) => index + ""}
                renderItem={({item})=>
                    <TouchableOpacity
                        onPress={()=>{
                            this.setState({text: item});
                            this.onSearch(item);
                        }}
                    >
                        <Text style={styles.textKeyWord}>{item}</Text>
                    </TouchableOpacity>}>
            </FlatList>
        )
    }

    onSearch=(text) => {
        let listResult = [];
        if(text!==''){
            let i = 0;
            for(i; i<data.length; i++){
                if(xoa_dau(data[i].name.toUpperCase()).indexOf(xoa_dau(text.toUpperCase()))!==-1){
                    listResult.push(data[i]);
                }
            }
        }
        this.setState({text, listData: listResult})
    };

    loadMore(){
        let listLoadMore = this.state.listData;
        let i = 0;
        for(i; i<data.length; i++){
            if(data[i].name.indexOf(this.state.text)!==-1){
                listLoadMore.push(data[i]);
            }
        }
        this.setState({listData: listLoadMore})
    }


    render() {
        return (
            <SafeAreaView>
                <View style={styles.containerSearchBox}>
                    <TextInput
                        style={styles.searchBox}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder={'Bạn muốn tìm kiếm sản phẩm gì?'}
                        onChangeText={this.onSearch}
                        value={this.state.text}
                    />
                    <TouchableOpacity style={styles.containerIconSearch}
                                      onPress={()=>{
                                          this.state.keyWord.push(this.state.text);
                                      }}
                    >
                        <Image style={styles.iconSearch} source={require('../assets/ic_search.png')}/>
                    </TouchableOpacity>

                </View>
                {
                    this.state.text===''?
                        this.historySearch()
                        :this.state.listData.length===0?
                            <View style={styles.emptyData}>
                                <Text style={styles.textEmpty}>Chưa có sản phẩm</Text>
                            </View>
                            :
                            <FlatList
                                style={styles.listProduct}
                                data={this.state.listData}
                                renderItem={this.renderItem}
                                keyExtractor={(item, index) => index + ""}
                                onEndReached={({distanceFromEnd}) => {
                                    if(distanceFromEnd>0){
                                        this.loadMore();
                                    }
                                }}
                            />

                }

            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    containerSearchBox:{
        margin: 10,
        borderRadius: 10,
        borderWidth: 1.5,
        borderColor: '#0c81f6',
        flexDirection: 'row',
        overflow: 'hidden'
    },
    searchBox: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        padding: 10,
    },
    containerIconSearch:{
        padding: 10,
        backgroundColor: '#0c81f6',
    },
    iconSearch:{
        width: 24,
        height: 24,
        tintColor: 'white'
    },
    emptyData:{
        justifyContent: 'center',
        alignItems: 'center',
    },
    containerItem:{
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
        padding: 10,
        backgroundColor: '#f8f8f8',
        marginBottom: 1,
    },
    thumbnailProduct:{
        width: 36,
        height: 36,
        resizeMode: 'contain'
    },
    textName:{
        fontSize: 13,
        flex: 1,
        padding: 10
    },
    textEmpty:{
        fontSize: 13,
        color: 'gray'
    },
    textPrice:{
        fontSize: 13,
        color: 'red',
        fontWeight: '400',
        padding: 10
    },
    listProduct:{
    },
    textKeyWord:{
        fontSize: 13,
        padding: 10,
        backgroundColor: '#f8f8f8',
        margin: 3,
    }

});

export default HomeScreen;