import React, {Component} from 'react';
import {
    WebView,
    StyleSheet,
    View
} from 'react-native';
import {SafeAreaView} from 'react-navigation';


class DetailsScreen extends Component {

    render() {
        const {params} = this.props.navigation.state;
        return (
                <View style={styles.webView}>
                    <WebView source={{uri: params.link}} style= {styles.webView}
                             javaScriptEnabled={true}
                             domStorageEnabled={true}
                             startInLoadingState={true}
                             scalesPageToFit={true}/>
                </View>
        );
    }
}

const styles = StyleSheet.create({
    webView: {
        flex: 1
    }
});


export default DetailsScreen;